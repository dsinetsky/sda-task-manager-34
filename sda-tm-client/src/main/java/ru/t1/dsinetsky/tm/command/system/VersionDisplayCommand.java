package ru.t1.dsinetsky.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.dsinetsky.tm.constant.ArgumentConst;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.dto.request.system.SystemVersionRequest;
import ru.t1.dsinetsky.tm.dto.response.system.SystemVersionResponse;

public final class VersionDisplayCommand extends AbstractSystemCommand {

    @NotNull
    public static final String ARGUMENT = ArgumentConst.CMD_VERSION;

    @NotNull
    public static final String NAME = TerminalConst.CMD_VERSION;

    @NotNull
    public static final String DESCRIPTION = "Shows program version";

    @Override
    public void execute() {
        @NotNull final SystemVersionResponse response = getSystemEndpoint().getVersion(new SystemVersionRequest());
        System.out.println(response.getVersion());
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

}
