package ru.t1.dsinetsky.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.constant.TerminalConst;

public final class UserHelpDisplayCommand extends AbstractSystemCommand {

    @Nullable
    public static final String ARGUMENT = null;

    @NotNull
    public static final String NAME = TerminalConst.CMD_USER_HELP;

    @NotNull
    public static final String DESCRIPTION = "Shows user commands";

    @Override
    public void execute() {
        listCommands(getCommandService().getUserCommands());
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @Nullable
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

}
