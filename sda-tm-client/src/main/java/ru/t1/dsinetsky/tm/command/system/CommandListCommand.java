package ru.t1.dsinetsky.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.dsinetsky.tm.api.model.ICommand;
import ru.t1.dsinetsky.tm.command.AbstractCommand;
import ru.t1.dsinetsky.tm.constant.ArgumentConst;
import ru.t1.dsinetsky.tm.constant.TerminalConst;

import java.util.Collection;

public final class CommandListCommand extends AbstractSystemCommand {

    @NotNull
    public static final String ARGUMENT = ArgumentConst.CMD_CMD;

    @NotNull
    public static final String NAME = TerminalConst.CMD_CMD;

    @NotNull
    public static final String DESCRIPTION = "Shows list of command names";

    @Override
    public void execute() {
        @NotNull final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (@NotNull final ICommand command : commands) {
            if (!command.getName().isEmpty())
                System.out.println(command.getName());
        }
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

}
