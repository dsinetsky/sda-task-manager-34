package ru.t1.dsinetsky.tm.dto.response.user;

import ru.t1.dsinetsky.tm.dto.response.AbstractResultResponse;

public final class UserLogoutResponse extends AbstractResultResponse {
}
