package ru.t1.dsinetsky.tm.dto.response.system;

import lombok.Getter;
import lombok.Setter;
import ru.t1.dsinetsky.tm.dto.response.AbstractResponse;

@Getter
@Setter
public final class SystemAboutResponse extends AbstractResponse {

    private String email;

    private String name;

}
