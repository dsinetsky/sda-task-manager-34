package ru.t1.dsinetsky.tm.exception.user;

public final class AccessDeniedException extends GeneralUserException {

    public AccessDeniedException() {
        super("Access denied! Please contact your system administrator.");
    }

    public AccessDeniedException(String message) {
        super("Access denied! Please contact your system administrator. Returned from " + message);
    }

}
