package ru.t1.dsinetsky.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.dsinetsky.tm.dto.request.user.admin.UserLockRequest;
import ru.t1.dsinetsky.tm.dto.request.user.admin.UserUnlockRequest;
import ru.t1.dsinetsky.tm.dto.response.user.admin.UserLockResponse;
import ru.t1.dsinetsky.tm.dto.response.user.admin.UserUnlockResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IAdminEndpoint extends IEndpoint {

    @NotNull
    String NAME = "AdminEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @WebMethod(exclude = true)
    static IAdminEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @WebMethod(exclude = true)
    static IAdminEndpoint newInstance(@NotNull final IConnectionProvider connectionProvider) {
        return IEndpoint.newInstance(connectionProvider, NAME, SPACE, PART, IAdminEndpoint.class);
    }

    @WebMethod(exclude = true)
    static IAdminEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        return IEndpoint.newInstance(host, port, NAME, SPACE, PART, IAdminEndpoint.class);
    }

    @NotNull
    @WebMethod
    @SneakyThrows
    UserLockResponse lockUserByLogin(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLockRequest request
    );

    @NotNull
    @SneakyThrows
    UserUnlockResponse unlockUserByLogin(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserUnlockRequest request
    );

}
