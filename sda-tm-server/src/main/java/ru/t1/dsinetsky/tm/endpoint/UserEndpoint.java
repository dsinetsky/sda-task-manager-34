package ru.t1.dsinetsky.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.api.endpoint.IUserEndpoint;
import ru.t1.dsinetsky.tm.api.service.IServiceLocator;
import ru.t1.dsinetsky.tm.api.service.IUserService;
import ru.t1.dsinetsky.tm.dto.request.user.UserChangePasswordRequest;
import ru.t1.dsinetsky.tm.dto.request.user.UserRegistryRequest;
import ru.t1.dsinetsky.tm.dto.request.user.UserUpdateEmailRequest;
import ru.t1.dsinetsky.tm.dto.request.user.UserUpdateNameRequest;
import ru.t1.dsinetsky.tm.dto.response.user.UserChangePasswordResponse;
import ru.t1.dsinetsky.tm.dto.response.user.UserRegistryResponse;
import ru.t1.dsinetsky.tm.dto.response.user.UserUpdateEmailResponse;
import ru.t1.dsinetsky.tm.dto.response.user.UserUpdateNameResponse;
import ru.t1.dsinetsky.tm.exception.user.AccessDeniedException;
import ru.t1.dsinetsky.tm.model.Session;
import ru.t1.dsinetsky.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.t1.dsinetsky.tm.api.endpoint.IUserEndpoint")
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    @NotNull
    private final IUserService userService = serviceLocator.getUserService();

    public UserEndpoint(final @NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public UserChangePasswordResponse changeUserPassword(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserChangePasswordRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String password = request.getPassword();
        @NotNull final User user = userService.changePassword(userId, password);
        return new UserChangePasswordResponse(user);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public UserRegistryResponse registryUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserRegistryRequest request
    ) {
        if (request == null) throw new AccessDeniedException();
        @Nullable final String login = request.getLogin();
        @Nullable final String password = request.getPassword();
        @NotNull final User user = serviceLocator.getAuthService().registry(login, password);
        return new UserRegistryResponse(user);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public UserUpdateEmailResponse updateUserEmail(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserUpdateEmailRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String email = request.getEmail();
        @Nullable final String userId = session.getUserId();
        @NotNull final User user = userService.updateEmailById(userId, email);
        return new UserUpdateEmailResponse(user);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public UserUpdateNameResponse updateUserName(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserUpdateNameRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String firstName = request.getFirstName();
        @Nullable final String lastName = request.getLastName();
        @Nullable final String middleName = request.getMiddleName();
        @Nullable final String userId = session.getUserId();
        @NotNull final User user = userService.updateUserById(userId, firstName, lastName, middleName);
        return new UserUpdateNameResponse(user);
    }

}
