package ru.t1.dsinetsky.tm.api.repository;

import ru.t1.dsinetsky.tm.model.Session;

public interface ISessionRepository extends IAbstractUserOwnedRepository<Session> {
}
