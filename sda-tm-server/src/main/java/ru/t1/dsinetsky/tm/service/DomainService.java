package ru.t1.dsinetsky.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import lombok.SneakyThrows;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.eclipse.persistence.jaxb.UnmarshallerProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.api.service.IDomainService;
import ru.t1.dsinetsky.tm.api.service.IServiceLocator;
import ru.t1.dsinetsky.tm.dto.Domain;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public final class DomainService implements IDomainService {

    @NotNull
    public static final String FILE_BASE64 = "./data.base64";

    @NotNull
    public static final String FILE_BINARY = "./data.bin";

    @NotNull
    public static final String FILE_XML = "./data.xml";

    @NotNull
    public static final String FILE_JSON = "./data.json";

    @NotNull
    public static final String FILE_BACKUP = "./data-backup.json";

    @NotNull
    public static final String FILE_YAML = "./data.yaml";

    @NotNull
    protected final String CONTEXT_FACTORY = "javax.xml.bind.context.factory";

    @NotNull
    protected final String CONTEXT_FACTORY_JAXB = "org.eclipse.persistence.jaxb.JAXBContextFactory";

    @NotNull
    protected final String APPLICATION_TYPE_JSON = "application/json";

    @NotNull
    private final IServiceLocator serviceLocator;


    public DomainService(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    public Domain getDomain() {
        @NotNull final Domain domain = new Domain();
        domain.setProjects(serviceLocator.getProjectService().returnAll());
        domain.setTasks(serviceLocator.getTaskService().returnAll());
        domain.setUsers(serviceLocator.getUserService().returnAll());
        return domain;
    }

    public void setDomain(@Nullable final Domain domain) throws GeneralException {
        if (domain == null) return;
        serviceLocator.getProjectService().set(domain.getProjects());
        serviceLocator.getTaskService().set(domain.getTasks());
        serviceLocator.getUserService().set(domain.getUsers());
    }

    @NotNull
    @Override
    @SneakyThrows
    public Domain saveDataBackup() {
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_BACKUP);
        @NotNull final Path path = file.toPath();
        Files.deleteIfExists(path);
        Files.createFile(path);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        @NotNull final ObjectMapper objectMapper = new JsonMapper();
        @NotNull final String data = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        fileOutputStream.write(data.getBytes(StandardCharsets.UTF_8));
        fileOutputStream.flush();
        fileOutputStream.close();
        return domain;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Domain loadDataBackup() {
        @NotNull final byte[] bytes = Files.readAllBytes(Paths.get(FILE_BACKUP));
        @NotNull final String data = new String(bytes);
        @NotNull final ObjectMapper objectMapper = new JsonMapper();
        @NotNull final Domain domain = objectMapper.readValue(data, Domain.class);
        setDomain(domain);
        return domain;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Domain saveDataBase64() {
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_BASE64);
        @NotNull final Path path = file.toPath();
        Files.deleteIfExists(path);
        Files.createFile(path);

        @NotNull final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();
        byteArrayOutputStream.close();

        @NotNull final byte[] bytes = byteArrayOutputStream.toByteArray();
        @NotNull final String base64 = new BASE64Encoder().encode(bytes);

        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(base64.getBytes(StandardCharsets.UTF_8));
        fileOutputStream.flush();
        fileOutputStream.close();
        return domain;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Domain loadDataBase64() {
        @NotNull final byte[] base64Byte = Files.readAllBytes(Paths.get(FILE_BASE64));
        @Nullable final String base64Data = new String(base64Byte);
        @Nullable final byte[] bytes = new BASE64Decoder().decodeBuffer(base64Data);
        @NotNull final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        objectInputStream.close();
        byteArrayInputStream.close();
        setDomain(domain);
        return domain;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Domain saveDataBinary() {
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_BINARY);
        @NotNull final Path path = file.toPath();
        Files.deleteIfExists(path);
        Files.createFile(path);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();
        fileOutputStream.close();
        return domain;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Domain loadDataBinary() {
        @NotNull final FileInputStream fileInputStream = new FileInputStream(FILE_BINARY);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        objectInputStream.close();
        fileInputStream.close();
        setDomain(domain);
        return domain;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Domain saveDataToJsonWithFasterXml() {
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_JSON);
        @NotNull final Path path = file.toPath();
        Files.deleteIfExists(path);
        Files.createFile(path);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        @NotNull final ObjectMapper objectMapper = new JsonMapper();
        @NotNull final String data = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        fileOutputStream.write(data.getBytes(StandardCharsets.UTF_8));
        fileOutputStream.flush();
        fileOutputStream.close();
        return domain;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Domain loadDataFromJsonWithFasterXml() {
        @NotNull final byte[] bytes = Files.readAllBytes(Paths.get(FILE_JSON));
        @NotNull final String data = new String(bytes);
        @NotNull final ObjectMapper objectMapper = new JsonMapper();
        @NotNull final Domain domain = objectMapper.readValue(data, Domain.class);
        setDomain(domain);
        return domain;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Domain saveDataToJsonWithJaxB() {
        System.setProperty(CONTEXT_FACTORY, CONTEXT_FACTORY_JAXB);
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_JSON);
        @NotNull final Path path = file.toPath();
        Files.deleteIfExists(path);
        Files.createFile(path);
        @NotNull JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        jaxbMarshaller.setProperty(MarshallerProperties.MEDIA_TYPE, APPLICATION_TYPE_JSON);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        jaxbMarshaller.marshal(domain, fileOutputStream);
        fileOutputStream.flush();
        fileOutputStream.close();
        return domain;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Domain loadDataFromJsonWithJaxB() {
        System.setProperty(CONTEXT_FACTORY, CONTEXT_FACTORY_JAXB);
        @NotNull JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        jaxbUnmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE, APPLICATION_TYPE_JSON);
        @NotNull final File file = new File(FILE_JSON);
        @NotNull final Domain domain = (Domain) jaxbUnmarshaller.unmarshal(file);
        setDomain(domain);
        return domain;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Domain saveDataToXmlWithFasterXml() {
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_XML);
        @NotNull final Path path = file.toPath();
        Files.deleteIfExists(path);
        Files.createFile(path);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @NotNull final String data = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        fileOutputStream.write(data.getBytes(StandardCharsets.UTF_8));
        fileOutputStream.flush();
        fileOutputStream.close();
        return domain;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Domain loadDataFromXmlWithFasterXml() {
        @NotNull final byte[] bytes = Files.readAllBytes(Paths.get(FILE_XML));
        @NotNull final String data = new String(bytes);
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @NotNull final Domain domain = objectMapper.readValue(data, Domain.class);
        setDomain(domain);
        return domain;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Domain saveDataToXmlWithJaxB() {
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_XML);
        @NotNull final Path path = file.toPath();
        Files.deleteIfExists(path);
        Files.createFile(path);
        @NotNull JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        jaxbMarshaller.marshal(domain, fileOutputStream);
        fileOutputStream.flush();
        fileOutputStream.close();
        return domain;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Domain loadDataFromXmlWithJaxB() {
        @NotNull JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        @NotNull final File file = new File(FILE_XML);
        @NotNull final Domain domain = (Domain) jaxbUnmarshaller.unmarshal(file);
        setDomain(domain);
        return domain;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Domain saveDataToYamlWithFasterXml() {
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_YAML);
        @NotNull final Path path = file.toPath();
        Files.deleteIfExists(path);
        Files.createFile(path);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        @NotNull final ObjectMapper objectMapper = new YAMLMapper();
        @NotNull final String data = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        fileOutputStream.write(data.getBytes(StandardCharsets.UTF_8));
        fileOutputStream.flush();
        fileOutputStream.close();
        return domain;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Domain loadDataFromYamlWithFasterXml() {
        @NotNull final byte[] bytes = Files.readAllBytes(Paths.get(FILE_YAML));
        @NotNull final String data = new String(bytes);
        @NotNull final ObjectMapper objectMapper = new YAMLMapper();
        @NotNull final Domain domain = objectMapper.readValue(data, Domain.class);
        setDomain(domain);
        return domain;
    }

}
