package ru.t1.dsinetsky.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.api.endpoint.IProjectEndpoint;
import ru.t1.dsinetsky.tm.api.service.IProjectService;
import ru.t1.dsinetsky.tm.api.service.IServiceLocator;
import ru.t1.dsinetsky.tm.dto.request.project.*;
import ru.t1.dsinetsky.tm.dto.response.project.*;
import ru.t1.dsinetsky.tm.enumerated.Status;
import ru.t1.dsinetsky.tm.model.Project;
import ru.t1.dsinetsky.tm.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService(endpointInterface = "ru.t1.dsinetsky.tm.api.endpoint.IProjectEndpoint")
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    @NotNull
    private final IProjectService projectService = serviceLocator.getProjectService();

    public ProjectEndpoint(final @NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public ProjectCreateResponse createProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectCreateRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final String userId = session.getUserId();
        @NotNull final Project project = projectService.create(userId, name, description);
        return new ProjectCreateResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public ProjectFindByIdResponse findProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectFindByIdRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String id = request.getId();
        @Nullable final String userId = session.getUserId();
        @NotNull final Project project = projectService.findById(userId, id);
        return new ProjectFindByIdResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public ProjectFindByIndexResponse findProjectByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectFindByIndexRequest request
    ) {
        @NotNull final Session session = check(request);
        final int index = request.getIndex();
        @Nullable final String userId = session.getUserId();
        @NotNull final Project project = projectService.findByIndex(userId, index);
        return new ProjectFindByIndexResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public ProjectListResponse listProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectListRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @NotNull final List<Project> projects = projectService.returnAll(userId);
        return new ProjectListResponse(projects);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public ProjectRemoveByIdResponse removeProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectRemoveByIdRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String id = request.getId();
        @Nullable final String userId = session.getUserId();
        @NotNull final Project project = serviceLocator.getProjectTaskService().removeProjectById(userId, id);
        return new ProjectRemoveByIdResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public ProjectRemoveByIndexResponse removeProjectByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectRemoveByIndexRequest request
    ) {
        @NotNull final Session session = check(request);
        final int index = request.getIndex();
        @Nullable final String userId = session.getUserId();
        @NotNull final Project project = serviceLocator.getProjectTaskService().removeProjectByIndex(userId, index);
        return new ProjectRemoveByIndexResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public ProjectStartByIdResponse startProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectStartByIdRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String id = request.getId();
        @NotNull final Status status = Status.IN_PROGRESS;
        @Nullable final String userId = session.getUserId();
        @NotNull final Project project = projectService.changeStatusById(userId, id, status);
        return new ProjectStartByIdResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public ProjectStartByIndexResponse startProjectByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectStartByIndexRequest request
    ) {
        @NotNull final Session session = check(request);
        final int index = request.getIndex();
        @NotNull final Status status = Status.IN_PROGRESS;
        @Nullable final String userId = session.getUserId();
        @NotNull final Project project = projectService.changeStatusByIndex(userId, index, status);
        return new ProjectStartByIndexResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public ProjectUpdateByIdResponse updateProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectUpdateByIdRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String id = request.getId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final String userId = session.getUserId();
        @NotNull final Project project = projectService.updateById(userId, id, name, description);
        return new ProjectUpdateByIdResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public ProjectUpdateByIndexResponse updateProjectByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectUpdateByIndexRequest request
    ) {
        @NotNull final Session session = check(request);
        final int index = request.getIndex();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final String userId = session.getUserId();
        @NotNull final Project project = projectService.updateByIndex(userId, index, name, description);
        return new ProjectUpdateByIndexResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public ProjectChangeStatusByIdResponse changeProjectStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectChangeStatusByIdRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String id = request.getId();
        @Nullable final Status status = request.getStatus();
        @Nullable final String userId = session.getUserId();
        @NotNull final Project project = projectService.changeStatusById(userId, id, status);
        return new ProjectChangeStatusByIdResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public ProjectChangeStatusByIndexResponse changeProjectStatusByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectChangeStatusByIndexRequest request
    ) {
        @NotNull final Session session = check(request);
        final int index = request.getIndex();
        @Nullable final Status status = request.getStatus();
        @Nullable final String userId = session.getUserId();
        @NotNull final Project project = projectService.changeStatusByIndex(userId, index, status);
        return new ProjectChangeStatusByIndexResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public ProjectClearResponse clearProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectClearRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        projectService.clear(userId);
        return new ProjectClearResponse();
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public ProjectCompleteByIdResponse completeProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectCompleteByIdRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String id = request.getId();
        @Nullable final Status status = Status.COMPLETED;
        @Nullable final String userId = session.getUserId();
        @NotNull final Project project = projectService.changeStatusById(userId, id, status);
        return new ProjectCompleteByIdResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public ProjectCompleteByIndexResponse completeProjectByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectCompleteByIndexRequest request
    ) {
        @NotNull final Session session = check(request);
        final int index = request.getIndex();
        @Nullable final Status status = Status.COMPLETED;
        @Nullable final String userId = session.getUserId();
        @NotNull final Project project = projectService.changeStatusByIndex(userId, index, status);
        return new ProjectCompleteByIndexResponse(project);
    }

}
