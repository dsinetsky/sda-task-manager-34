package ru.t1.dsinetsky.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.api.service.IServiceLocator;
import ru.t1.dsinetsky.tm.dto.request.AbstractUserRequest;
import ru.t1.dsinetsky.tm.enumerated.Role;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.exception.user.AccessDeniedException;
import ru.t1.dsinetsky.tm.model.Session;

public abstract class AbstractEndpoint {

    @NotNull
    protected final IServiceLocator serviceLocator;

    public AbstractEndpoint(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    protected Session check(@Nullable final AbstractUserRequest request) throws AccessDeniedException {
        if (request == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException();
        return serviceLocator.getAuthService().validateToken(token);
    }

    protected Session checkRole(@Nullable final AbstractUserRequest request, @Nullable final Role role) throws GeneralException {
        if (request == null) throw new AccessDeniedException();
        if (role == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        @NotNull final Session session = serviceLocator.getAuthService().validateToken(token);
        @Nullable final Role sessionRole = session.getRole();
        if (sessionRole == null) throw new AccessDeniedException();
        if (!sessionRole.equals(role)) throw new AccessDeniedException();
        return session;
    }

}
