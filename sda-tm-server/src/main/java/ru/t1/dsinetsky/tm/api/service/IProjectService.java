package ru.t1.dsinetsky.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.enumerated.Status;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.model.Project;

public interface IProjectService extends IAbstractUserOwnedService<Project> {

    @NotNull
    Project create(@Nullable String userId, @Nullable String name) throws GeneralException;

    @NotNull
    Project create(@Nullable String userId, @Nullable String name, @Nullable String desc) throws GeneralException;

    @NotNull
    Project updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String desc) throws GeneralException;

    @NotNull
    Project updateByIndex(@Nullable String userId, int index, @Nullable String name, @Nullable String desc) throws GeneralException;

    @NotNull
    Project changeStatusById(@Nullable String userId, String id, @Nullable Status status) throws GeneralException;

    @NotNull
    Project changeStatusByIndex(@Nullable String userId, int index, @Nullable Status status) throws GeneralException;

}
