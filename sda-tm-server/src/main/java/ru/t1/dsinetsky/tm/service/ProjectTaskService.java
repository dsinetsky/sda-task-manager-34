package ru.t1.dsinetsky.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.api.repository.IProjectRepository;
import ru.t1.dsinetsky.tm.api.repository.ITaskRepository;
import ru.t1.dsinetsky.tm.api.service.IProjectTaskService;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.exception.entity.ProjectNotFoundException;
import ru.t1.dsinetsky.tm.exception.entity.TaskNotFoundException;
import ru.t1.dsinetsky.tm.exception.field.IndexOutOfSizeException;
import ru.t1.dsinetsky.tm.exception.field.NegativeIndexException;
import ru.t1.dsinetsky.tm.exception.field.ProjectIdIsEmptyException;
import ru.t1.dsinetsky.tm.exception.field.TaskIdIsEmptyException;
import ru.t1.dsinetsky.tm.exception.user.UserNotLoggedException;
import ru.t1.dsinetsky.tm.model.Project;
import ru.t1.dsinetsky.tm.model.Task;

import java.util.List;

public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final IProjectRepository projectRepository;

    @NotNull
    private final ITaskRepository taskRepository;

    public ProjectTaskService(@NotNull final IProjectRepository projectRepository, @NotNull final ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public void bindProjectById(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskId) throws GeneralException {
        if (userId == null || userId.isEmpty()) throw new UserNotLoggedException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdIsEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdIsEmptyException();
        if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
        if (!taskRepository.existsById(userId, taskId)) throw new TaskNotFoundException();
        @Nullable final Task task = taskRepository.findById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
    }

    @Override
    public void unbindProjectById(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskId) throws GeneralException {
        if (userId == null || userId.isEmpty()) throw new UserNotLoggedException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdIsEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdIsEmptyException();
        if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
        if (!taskRepository.existsById(userId, taskId)) throw new TaskNotFoundException();
        @Nullable final Task task = taskRepository.findById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(null);
    }

    @Override
    @Nullable
    public Project removeProjectById(@Nullable final String userId, @Nullable final String projectId) throws GeneralException {
        if (userId == null || userId.isEmpty()) throw new UserNotLoggedException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdIsEmptyException();
        if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
        @Nullable final Project project = projectRepository.findById(userId, projectId);
        @NotNull final List<Task> tasks = taskRepository.findTasksByProjectId(userId, projectId);
        for (@NotNull final Task task : tasks) taskRepository.removeById(userId, task.getId());
        projectRepository.removeById(userId, projectId);
        return project;
    }

    @Override
    @NotNull
    public Project removeProjectByIndex(@Nullable final String userId, final int index) throws GeneralException {
        if (userId == null || userId.isEmpty()) throw new UserNotLoggedException();
        if (index < 0) throw new NegativeIndexException();
        if (index >= projectRepository.getSize(userId))
            throw new IndexOutOfSizeException(projectRepository.getSize(userId));
        @Nullable final Project project = projectRepository.findByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        @NotNull final List<Task> tasks = taskRepository.findTasksByProjectId(userId, project.getId());
        for (@NotNull final Task task : tasks) taskRepository.removeById(userId, task.getId());
        projectRepository.removeByIndex(userId, index);
        return project;
    }

}
